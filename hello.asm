global start

section .text

start:
	mov	rax, 0x2000004
	mov	rdi, 1
	mov	rsi, hello_world
	mov	rdx, hello_world.length
	syscall

	mov	rax, 0x2000001
	mov	rdi, 0
	syscall

section .data
	hello_world:	db "Hello World!", 0xa
	.length:		equ $-hello_world

	var1: db 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88

